<?php

namespace SUPREMATIK\PLATFORM\Model\ResourceModel\HistoryLog;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package SUPREMATIK\PLATFORM\Model\ResourceModel\HistoryLog
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'log_id';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(
            \SUPREMATIK\PLATFORM\Model\HistoryLog::class,
            \SUPREMATIK\PLATFORM\Model\ResourceModel\HistoryLog::class
        );
    }
}
