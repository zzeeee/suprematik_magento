<?php

namespace SUPREMATIK\PLATFORM\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class HistoryLog
 * @package SUPREMATIK\PLATFORM\Model\ResourceModel
 */
class HistoryLog extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('suprematik_history_log', 'log_id');
    }
}
