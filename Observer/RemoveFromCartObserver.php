<?php

namespace SUPREMATIK\PLATFORM\Observer;

use SUPREMATIK\PLATFORM\Provider\GeneralSettings;
use SUPREMATIK\PLATFORM\Service\RemoveFromCart;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use SUPREMATIK\PLATFORM\Service\AddToCart;

/**
 * Class RemoveFromCartObserver
 * @package SUPREMATIK\PLATFORM\Observer
 */
class RemoveFromCartObserver implements ObserverInterface
{
    /**
     * @var RemoveFromCart
     */
    protected $removeFromCart;

    /**
     * @var GeneralSettings
     */
    protected $generalSettings;
    private AddToCart $addToCart;

    /**
     * RemoveFromCartObserver constructor.
     * @param RemoveFromCart $removeFromCart
     * @param GeneralSettings $generalSettings
     */
    public function __construct(
        RemoveFromCart $removeFromCart,
        AddToCart                $addToCart,

        GeneralSettings $generalSettings
    ) {
        $this->removeFromCart = $removeFromCart;
        $this->addToCart = $addToCart;
        $this->generalSettings = $generalSettings;
    }

    /**
     * @param Observer $observer
     * @return $this|void
     */
    public function execute(Observer $observer)
    {
      //  echo('deldel');
        if (!$this->generalSettings->cartManagementEnabled()) {
         //   return $this;
        }

        $item = $observer->getData('quote_item');
        $quote = $item->getQuote();
       // $this->removeFromCart->execute($quote, $item);
        $this->addToCart->removeFromCart($quote, $item);
        return $this;
    }
}
