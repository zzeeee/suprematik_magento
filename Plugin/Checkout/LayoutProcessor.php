<?php
namespace SUPREMATIK\PLATFORM\Plugin\Checkout;

use SUPREMATIK\PLATFORM\Provider\GeneralSettings;

/**
 * Class LayoutProcessor
 * @package SUPREMATIK\PLATFORM\Plugin\Checkout
 */
class LayoutProcessor
{
    const SHIPPIN_STEP = 'preview_the_order';
    const BILLING_STEP = 'after_order_record';

    /**
     * @var GeneralSettings
     */
    protected $generalSettings;

    /**
     * LayoutProcessor constructor.
     * @param GeneralSettings $generalSettings
     */
    public function __construct(
        GeneralSettings $generalSettings
    ) {
        $this->generalSettings = $generalSettings;
    }

    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {

    }
}
