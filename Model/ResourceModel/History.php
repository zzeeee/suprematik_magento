<?php

namespace SUPREMATIK\PLATFORM\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class History
 * @package SUPREMATIK\PLATFORM\Model\ResourceModel
 */
class History extends AbstractDb
{
    protected function _construct()
    {
   // echo("T!");
        $this->_init('suprematik_history', 'history_id');
    }
}
