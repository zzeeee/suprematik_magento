<?php

namespace SUPREMATIK\PLATFORM\Logger;

/**
 * Class Handler
 * @package SUPREMATIK\PLATFORM\Logger
 */
class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * @var string
     */
    protected $fileName = '/var/log/suprematik-integration.log';

    /**
     * @var int
     */
    protected $loggerType = \Monolog\Logger::DEBUG;
}
