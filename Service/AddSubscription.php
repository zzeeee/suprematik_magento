<?php

namespace SUPREMATIK\PLATFORM\Service;

use SUPREMATIK\PLATFORM\Model\Api\DTO\Customer as CustomerDTO;
use SUPREMATIK\PLATFORM\Model\History\Action;
use SUPREMATIK\PLATFORM\Model\History\Status;
use SUPREMATIK\PLATFORM\Model\HistoryFactory;
use SUPREMATIK\PLATFORM\Model\HistoryProcess;
use SUPREMATIK\PLATFORM\Model\HistoryRepository;
use SUPREMATIK\PLATFORM\Model\Rest\HttpClient;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Webapi\Rest\Request;
use SUPREMATIK\PLATFORM\Logger\Integration as LoggerIntegration;

/**
 * Class AddSubscription
 * @package SUPREMATIK\PLATFORM\Service
 */
class AddSubscription
{
    const SUBSCRIPTION_ENDPOINT = '/users/subscription';
    const SUBSCRIPTION_TYPE = 1;

    /**
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * @var CustomerDTO
     */
    protected $customerDto;

    /**
     * @var HistoryProcess
     */
    protected $processHistory;

    /**
     * @var HistoryFactory
     */
    protected $historyFactory;

    /**
     * @var HistoryRepository
     */
    protected $historyRepository;

    /**
     * @var Json
     */
    protected $json;

    /**
     * AddSubscription constructor.
     * @param HttpClient $httpClient
     * @param HistoryProcess $processHistory
     * @param CustomerDTO $customerDto
     * @param HistoryFactory $historyFactory
     * @param HistoryRepository $historyRepository
     * @param Json $json
     */
    public function __construct(
        HttpClient $httpClient,
        HistoryProcess $processHistory,
        CustomerDTO $customerDto,
        HistoryFactory $historyFactory,
        HistoryRepository $historyRepository,
        Json $json
    ) {
        $this->httpClient = $httpClient;
        $this->customerDto = $customerDto;
        $this->processHistory = $processHistory;
        $this->historyFactory = $historyFactory;
        $this->historyRepository = $historyRepository;
        $this->json = $json;
    }

    /**
     * @param $data
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function process($data)
    {
        $params['body'] = $data;
        $response = $this->httpClient->doRequest(
            self::SUBSCRIPTION_ENDPOINT,
            $params,
            Request::HTTP_METHOD_POST
        );
        $result = $response->getBody()->getContents();
    }

    /**
     * @param CustomerInterface $customer
     */
    public function execute(CustomerInterface $customer)
    {
        try {
            $subscriber = [
                'userInfo' => $this->customerDto->getUserInfoData($customer),
                'subscriptionType' => self::SUBSCRIPTION_TYPE
            ];

            $modelData = $this->json->serialize($subscriber);
            $history = $this->historyFactory->create();
            $history->setStatus(Status::PENDING)
                ->setAction(Action::ADD_SUBSCRIPTION)
                ->setServiceClass(AddSubscription::class)
                ->setEntityData($modelData);
            $history = $this->historyRepository->save($history);
            $this->processHistory->processById($history->getHistoryId());
        } catch (\Exception $e) {
            $e->getMessage();
        }
    }
}
