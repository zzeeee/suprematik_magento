<?php

namespace SUPREMATIK\PLATFORM\Controller\Adminhtml\History;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index
 * @package SUPREMATIK\PLATFORM\Controller\Adminhtml\History
 */
class Index extends Action
{
    const ADMIN_RESOURCE = 'SUPREMATIK_PLATFORM::history';
    const ACTIVE_MENU = 'SUPREMATIK_PLATFORM::history';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
      //  echo('AAAAAA');
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);

    }

    /**
     * @return ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();

        return $resultPage;
    }
}
