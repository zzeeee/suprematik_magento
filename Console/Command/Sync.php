<?php

namespace SUPREMATIK\PLATFORM\Console\Command;

use SUPREMATIK\PLATFORM\Logger\Integration as LoggerIntegration;
use SUPREMATIK\PLATFORM\Model\Rest\HttpClient;

// use SUPREMATIK\PLATFORM\Service\ExportHistorical as SyncService;
use SUPREMATIK\PLATFORM\Model\Api\DTO\Product as ProductDTO;
use SUPREMATIK\PLATFORM\Model\Api\MainService as MainVisionService;
use SUPREMATIK\PLATFORM\Provider\GeneralSettings;
use SUPREMATIK\PLATFORM\Service\MainVision;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\State;
use Magento\Framework\Console\Cli;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\FlagManager;
use Magento\Store\Model\StoreManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Sync
 * @package SUPREMATIK\Sync\Console\Command
 */
class Sync extends Command
{
    protected $state;
    protected $sync;
    protected string $routUrl = '';
    protected string $magentoHelperUrl = '';
    protected $logger;
    protected $mainVisionService;
    protected $magentoId;
    protected $flagManager;
    protected $objectManager;
    private HttpClient $httpClient;
    private GeneralSettings $generalSettings;

    /**
     * Sync constructor.
     * @param State $state
     * @param LoggerIntegration $logger
     * @param string|null $name
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface         $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        //  State $state,
        FlagManager                                        $flagManager,
        HttpClient $httpClient,

        //  SyncService $sync,
        LoggerIntegration                                  $logger,
        StoreManager                                       $StoreManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,

        GeneralSettings                                    $generalSettings,/*
        ProductCollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Api\SearchCriteriaInterface $criteria,
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        ProductDTO $productDto, */
        MainVisionService                                  $mainVisionService,

        //    \SUPREMATIK\PLATFORM\Service\MainVision $mainVision,
        // string $name = null
    )
    {
        $this->flagManager = $flagManager;
        $flagCode = 'suprematik_magento_id';
        $this->objectManager = $objectManager;
        $this->httpClient = $httpClient;
        $this->generalSettings = $generalSettings;

        $this->magentoId = $this->flagManager->getFlagData($flagCode);


        $this->mainVisionService = $mainVisionService;
        //   $this->sync = $sync;
        $this->logger = $logger;
        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('suprematik:export');
        $this->setDescription('Suprematik Export');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws LocalizedException
     */


    protected function execute(InputInterface $input, OutputInterface $output)
    {
     //   $this->sendLog();

        $output->writeln('<info>Start sync DB process</info>');
        $res = $this->mainVisionService->doSyncDataRegular();

        if ($res)             return Cli::RETURN_SUCCESS;
        return Cli::RETURN_FAILURE;

    }


}
