<?php

namespace SUPREMATIK\PLATFORM\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface HistoryLogSearchResultsInterface
 * @package SUPREMATIK\PLATFORM\Api\Data
 */
interface HistoryLogSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return HistoryLogInterface[]
     */
    public function getItems();

    /**
     * @param HistoryLogInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
