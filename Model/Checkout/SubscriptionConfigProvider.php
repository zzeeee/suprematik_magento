<?php

namespace SUPREMATIK\PLATFORM\Model\Checkout;

use SUPREMATIK\PLATFORM\Provider\GeneralSettings;
use Magento\Checkout\Model\ConfigProviderInterface;

/**
 * Class SubscriptionConfigProvider
 * @package SUPREMATIK\PLATFORM\Model\Checkout
 */
class SubscriptionConfigProvider implements ConfigProviderInterface
{
    /**
     * @var array
     */
    protected $checkoutPages = [
        'checkout',
        'both'
    ];

    /**
     * @var GeneralSettings
     */
    protected $generalSettings;

    /**
     * SubscriptionConfigProvider constructor.
     * @param GeneralSettings $generalSettings
     */
    public function __construct(
        GeneralSettings $generalSettings
    ) {
        $this->generalSettings = $generalSettings;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return [
            'suprematik' => [

            ]
        ];
    }
}
